
<!-- README.md is generated from README.Rmd. Please edit that file -->

## Overview

This repository contains code used in solutions to the exerices from [An
Introduction to Statistical Learning with Applications in
R](https://www.statlearning.com/) by Gareth James, Daniela Witten,
Trevor Hastie and Robert Tibshirani.

## Chapters

[Statistical
Learning](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/isl/ch02)

[Linear
Regression](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/isl/ch03)

[Classification](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/isl/ch04)

[Resampling
Methods](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/isl/ch05)

[Linear Model Selection and
Regularization](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/isl/ch06)

Moving Beyond Linearity

Tree-Based Methods

Support Vector Machines

Unsupervised Learning
