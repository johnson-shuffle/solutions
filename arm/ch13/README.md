Multilevel linear models: varying slopes, non-nested models, and other
complexities
================

-   [Question 1](#question-1)
-   [Question 2](#question-2)

## Question 1

``` r
# reload chapter 3, exercise 5
dat <- knitr::load_cache('arm03-q05_', 'dat')
```

#### (a)

The model is given by:

``` math
\begin{align}
y_i &\sim \text{N} \left( \alpha_{j[i]} + \beta_{j[i]} x_i + Z_i \Pi, \sigma_y^2 \right) \\
\begin{matrix}
  \alpha_j \\
  \beta_j
\end{matrix} &\sim \text{N}
\left(
  \begin{matrix}
  \mu_{\alpha} \\
  \mu_{\beta}
  \end{matrix},
  \begin{pmatrix}
  \sigma_{\alpha}^2 & \rho \sigma_{\alpha} \sigma_{\beta} \\
  \rho \sigma_{\alpha} \sigma_{\beta} & \sigma_{\beta}^2
  \end{pmatrix}
\right)
\end{align}
```

where $`x_i`$ is a measure of the professor’s beauty and $`Z_i`$
contains indicators for whether the professor is female and whether the
professor is a minority. In this case, the grouping variable course is
created using the class\* indicators; data with no indicator were
dropped.

### (b)

``` r
# rename outcome variable and create id
dat %<>%
  rename(ce = courseevaluation) %>%
  mutate(
    id = 1:n()
  )

# create course category?
category <- select(dat, id, starts_with("class"))
category %<>% gather(course, value, -id)
category %<>% filter(value != 0)
category$course %<>% str_remove_all("class")
category$course %<>% as.numeric()

# join back category
dat %<>% left_join(select(category, id, course), by = "id")

# drop course = NA
dat %<>% filter(!is.na(course))

# fit the model
reg <- lmer(
  ce ~ btystdave + female + minority + (1 + btystdave | course),
  data = dat
  )
summary(reg)
```

    ## Linear mixed model fit by REML ['lmerMod']
    ## Formula: ce ~ btystdave + female + minority + (1 + btystdave | course)
    ##    Data: dat
    ## 
    ## REML criterion at convergence: 229.2
    ## 
    ## Scaled residuals: 
    ##      Min       1Q   Median       3Q      Max 
    ## -2.29019 -0.72870  0.04906  0.62117  1.84444 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev. Corr
    ##  course   (Intercept) 0.11100  0.3332       
    ##           btystdave   0.02865  0.1693   0.59
    ##  Residual             0.18102  0.4255       
    ## Number of obs: 157, groups:  course, 30
    ## 
    ## Fixed effects:
    ##             Estimate Std. Error t value
    ## (Intercept)  4.11545    0.08248  49.899
    ## btystdave    0.08987    0.07272   1.236
    ## female      -0.17832    0.08841  -2.017
    ## minority    -0.31482    0.09964  -3.159
    ## 
    ## Correlation of Fixed Effects:
    ##           (Intr) btystd female
    ## btystdave  0.356              
    ## female    -0.341 -0.002       
    ## minority  -0.131 -0.027 -0.278

In this model, the unexplained within-course variation has an estimated
standard deviation of
![\\sigma\_{ce} = 0.43](http://chart.apis.google.com/chart?cht=tx&chl=%5Csigma_%7Bce%7D%20%3D%200.43 "\sigma_{ce} = 0.43"),
the estimated standard deviation of the course intercepts is
![\\sigma\_{course} = 0.33](http://chart.apis.google.com/chart?cht=tx&chl=%5Csigma_%7Bcourse%7D%20%3D%200.33 "\sigma_{course} = 0.33")
and the estimated standard deviation of the course slopes is
![\\sigma\_{bystdave} = 0.17](http://chart.apis.google.com/chart?cht=tx&chl=%5Csigma_%7Bbystdave%7D%20%3D%200.17 "\sigma_{bystdave} = 0.17").

### (c)

``` r
# coefficients
dat_coef <- coef(reg)$course %>%
  rownames_to_column(var = "course") %>%
  mutate_at(vars(course), as.numeric)

# create new grouping
dat %<>%
  mutate(
    female_minority = case_when(
      female == 0 & minority == 0 ~ "White male",
      female == 0 & minority == 1 ~ "Minority male",
      female == 1 & minority == 0 ~ "White female",
      TRUE ~ "Minority female"
    )
  )
  
# plot
p <- ggplot() +
  geom_point(
    aes(x = btystdave, y = ce, colour = female_minority),
    data = dat
  ) +
  geom_abline(
    aes(intercept = `(Intercept)`, slope = btystdave),
    data = dat_coef
  ) +
  scale_colour_ipsum() +
  theme_ipsum() +
  labs(x = "Beauty score", y = "Evaluation score", colour = "Type") +
  facet_wrap(~course)
p
```

![](../arm_fig/arm13-q01c-1-1.png)<!-- -->

``` r
pdat <- bind_cols(
  coef(reg)$course[, 1], 
  ranef(reg)$course[, 1],
  .name_repair = "unique"
)
names(pdat) <- c("est", "se")

p <- ggplot(pdat, aes(x = factor(1:30), colour = factor(1:30))) +
  geom_point(aes(y = est)) +
  geom_errorbar(aes(ymin = est - 2 * se, ymax = est + 2 * se)) +
  theme_ipsum() +
  labs(x = "Course", y = "Intercept") +
  guides(colour = F)
p
```

![](../arm_fig/arm13-q01c-2-1.png)<!-- -->

``` r
pdat <- bind_cols(
  coef(reg)$course[, 2], 
  ranef(reg)$course[, 2],
  .name_repair = "unique"
)
names(pdat) <- c("est", "se")

p <- ggplot(pdat, aes(x = factor(1:30), colour = factor(1:30))) +
  geom_point(aes(y = est)) +
  geom_errorbar(aes(ymin = est - 2 * se, ymax = est + 2 * se)) +
  theme_ipsum() +
  labs(x = "Course", y = "Slope") +
  guides(colour = F)
p
```

![](../arm_fig/arm13-q01c-3-1.png)<!-- -->

## Question 2

### (a)

### Question 3

``` r
# reload chapter 3, exercise 5
dat <- knitr::load_cache('arm11-q03_', 'dat')
```

### (a)
