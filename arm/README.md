
<!-- README.md is generated from README.Rmd. Please edit that file -->

## Overview

This repository contains code used in solutions to the exerices from
[Data Analysis Using Regression and Multilevel/Hierarchical
Models](http://www.stat.columbia.edu/~gelman/arm/) by Andrew Gelman and
Jennifer Hill.

## Chapters

[Concepts and methods from basic probability and
statistics](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/arm/ch02)

[Linear regression: the
basics](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/arm/ch03)

[Linear regression: before and after fitting the
model](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/arm/ch04)

[Logistic
regression](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/arm/ch05)

[Generalized linear
models](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/arm/ch06)

[Simulation of probability models and statistical
inferences](hhttps://gitlab.com/johnson-shuffle/solutions/-/tree/main/arm/ch07)

[Simulation for checking statistical procedures and model
fits](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/arm/ch08)

[Causal inference using regression on the treatment
variable](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/arm/ch09)

[Causal inference using more advanced
models](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/arm/ch10)

[Multilevel
structures](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/arm/ch11)

[Multilevel linear models: the
basics](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/arm/ch12)

[Multilevel linear models: varying slopes, non-nested models, and other
complexities](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/arm/ch13)

Multilevel logistic regression

Multilevel generalized linear models

Multilevel modeling in Bugs and R: the basics

Fitting multilevel linear and generalized linear models in Bugs and R

Likelihood and Bayesian inference and computation

Debugging and speeding convergence

Sample size and power calculations

Understanding and summarizing the fitted models

Analysis of variance

Causal inference using multilevel models

Model checking and comparison

Missing-data imputation
