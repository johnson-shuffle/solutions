
<!-- README.md is generated from README.Rmd. Please edit that file -->

## Overview

This repository contains the code used in solutions to the exerices from
the textbooks:

-   [Data Analysis Using Regression and Multilevel/Hierarchical
    Models](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/arm)

-   [An Introduction to Statistical Learning: With Applications in
    R](https://gitlab.com/johnson-shuffle/solutions/-/tree/main/isl)

Note that in many cases the **solutions are incomplete** and **mistakes
are practically guaranteed**.
